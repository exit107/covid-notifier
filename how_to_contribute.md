# Contributing

## How to setup a development server
1.  Clone the repo
	* `git clone https://github.com/exit107/covid-notifier.git`
2. Change into the repo directory
	* `cd covid-notifier`
4. Build the environment files
    * `cp ./env/db.env.example ./env/db.env`
    * `sed 's/^/export /g' ./env/frontend.env.example > ./env/frontend.dev.env`
    * `sed -i 's/@db/@localhost/g ./env/frontend.dev.env`
    * `source ./env/frontend.dev.env`
3. Start the database
    * `docker-compose build && docker-compose run -d --name dev_db --no-deps --rm -p 5432:5432 db`
2. Change into the frontend directory
	* `cd frontend`
5. Install requirements and activate your environment.
    * `poetry install && poetry shell`
6. Initialize the database
    * `flask db upgrade && flask pull-updates`
7. Start the development server
    * `flask run`
8. Hack away!!!

## Any other advice?
Not really. I'm a rather inexperienced Python programmer who just does fun stuff like this in his free time. If you have thoughts on how I can be better as a programmer or person in general, please email me, Thanks!
